"""This script creates views of canadian census CMAs at the province geography
the CMAs are merged on cmauid and cross province boundaries.

the view stats_canads<YYYY>.cn<YY>cm must exist, so can_cens.py must run first"""

import sys, psycopg2, re, subprocess
import base, can_cens_queries
from multiprocessing import Pool
from can_cens import control_dict, census_year

version = sys.argv[1] #'2017.03'
da_table_name = sys.argv[2] #'mn_unshorlined_2201612'
dump_dir = sys.argv[3] #r'\\tro-share01\datawarehouse\warehouse\PB-InhouseDemographicData\Canada\Geography\C11_TOMTOM_BOUNDARIES\test_dump'
layer_control_file = sys.argv[4] #r'\\prodfs01.maponics.local\Resource-Library\Processing\can_cens_unshor_layer_control.txt'

#execute the querys and write each resulting view to a TAB file using ogr2ogr
def run_query(query):
    with psycopg2.connect(dbname=control_dict['database'], port='5432', user='ace_procs', host=control_dict['host'], password='nosecurity') as connection:
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
    name = re.search('\.(.*?) AS', query).group(1)
    print('View ' + name + ': Created')

    #NOTE! the PG argument of the ogr2ogr control string needs '' for use in linux example: dbname='ace'
    pg_string = """PG:"dbname=ace host={0} port=5432 user=ace_procs password=nosecurity """.format(control_dict['host'])
    pg_string += 'schemas={0}"'.format(control_dict['schema'])
    cmd_list = ['set', 'PGCLIENTENCODING=LATIN1&ogr2ogr', '-f', '"MapInfo File"', dump_dir, pg_string, name] #remove 'set' when runing on linux

    cmd = " ".join(cmd_list)
    print(cmd)
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    p.wait()
    rc = p.returncode

    if rc != 0:
        print(name + ' !!!!!!failed to convert!!!!!!!')
    else:
        print(name + '.TAB Created')

#function to update the tab_list.txt file from query_list2
def update_tab_list(inList, dump_dir):
    outfile = dump_dir + r'\tab_list.txt'
    with open(outfile, 'a') as tf:
        for x in inList:
            tab_name = re.search('\.(.*?) AS', x).group(1)
            tab_path = dump_dir + '\\' + tab_name + '.TAB'
            tf.write("{}\n".format(tab_path))


#run the main function in a pool
def main():
    do_list2 = base.text_to_list(layer_control_file)
    query_list2 = can_cens_queries.generate_can_cens_query_list2(do_list2, control_dict, version, census_year)
    update_tab_list(query_list2, dump_dir)
    pool = Pool(4)
    pool.map(run_query, query_list2)
    pool.close()
    pool.join()

if __name__ == '__main__':
    main()
