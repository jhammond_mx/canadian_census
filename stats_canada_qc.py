import set_python_path
import sys
from util import control
import base

control_file = sys.argv[1]
control_dict = control.get_control_dict(control_file)
year = sys.argv[2]
raw_data_dir = sys.argv[3]

shp_list = base.make_list_of_shp(raw_data_dir)
error_list = []

if __name__ == "__main__":
    for shp_file in shp_list:
        table_name, pkey_name = base.get_shp_info(shp_file)
        if table_name == None or table_name == 'canada_roads':
            #skip the roads any shapefiles that we didn't load
            continue

        print('\n')
        print('QC TESTING ON: -------------------- ' + table_name + ' ---------------------------')
        print('QC TEST: make sure all features loaded ----------------- ')
        featureCount = base.get_shp_feature_count(shp_file)
        row_count_query = """SELECT COUNT(*) FROM {0}.{1}{2};
            """.format(control_dict['schema'], table_name, year)
        row_count_raw = base.run_db_query_get_results(row_count_query, control_dict)
        row_count = row_count_raw[0][0][0]
        row_count_msg = 'Number of rows in table ' + table_name + year + ': '
        print(row_count_msg, row_count)
        if featureCount != row_count:
            print(table_name, year, ' MISSING FEATURES!!!')
            sys.exit(1)

        print('QC TEST: check for valid geometry ---------------------round 1')
        #run the first st_isValid() test
        error_ids_query, error_sub_list, error_id_list = base.geometry_qc_test(control_dict, table_name, year, pkey_name)
        if len(error_id_list) > 0:
            make_valid_query = """UPDATE {0}.{1}{2}
                SET geom = ST_MakeValid(geom) WHERE {3};
                """.format(control_dict['schema'], table_name, year, error_ids_query)
            print('ATTEMPTING GEOMETRY FIX: ', make_valid_query)
            base.run_db_query(make_valid_query, control_dict)

            print('QC TEST: check for valid geometry ---------------------round 2')
            #run the second st_isValid() test if necessary
            error_ids_query, error_sub_list, error_id_list = base.geometry_qc_test(control_dict, table_name, year, pkey_name)
            if len(error_id_list) > 0:
                buffer_query = """UPDATE {0}.{1}{2}
                SET geom = ST_Multi(ST_Buffer(geom, 0)) WHERE {3};
                """.format(control_dict['schema'], table_name, year, error_ids_query)
                print('ATTEMPTING GEOMETRY FIX: ', buffer_query)
                base.run_db_query(buffer_query, control_dict)

                #run the third st_isValid() test if necessary
                error_ids_query, error_sub_list, error_id_list = base.geometry_qc_test(control_dict, table_name, year, pkey_name)
                if len(error_id_list) > 0:
                    print("QC TEST FAILED")
                    sys.exit(1)
                else:
                    print('qc: passed')
            else:
                print('qc: passed')
        else:
            print('qc: passed')
        error_list += error_sub_list

    print(error_list)
    if len(error_list) > 0:
        base.make_error_pts_shp(raw_data_dir, error_list)
        sys.exit(0)
    else:
        sys.exit(0)

