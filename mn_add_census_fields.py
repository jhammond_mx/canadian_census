"""This script appends stats canada data for dissemination area to TomTom based dissemination area data"""

import sys, base

control_dict = {'schema':'mn_canadian_census', 'database':'ace', 'port':'5432', 'user':'ace_procs', 'host':'prod-adb01.maponics.local', 'password':'nosecurity'}
table_name = sys.argv[1] #'mn_unshorlined_2201612'
shp_file = sys.argv[2]
census_year = '2011' #this is version of Stats Canada data used, this determins the uid's that roll up the DA's to higher census geographies

#drop fields/column from previous run if necessary
clear_fields_query = """ALTER TABLE {0}.{1}
    DROP COLUMN IF EXISTS cduid,
    DROP COLUMN IF EXISTS cdname,
    DROP COLUMN IF EXISTS csduid,
    DROP COLUMN IF EXISTS csdname,
    DROP COLUMN IF EXISTS cmauid,
    DROP COLUMN IF EXISTS cmaname,
    DROP COLUMN IF EXISTS ctuid,
    DROP COLUMN IF EXISTS ctname,
    DROP COLUMN IF EXISTS pruid,
    DROP COLUMN IF EXISTS prname;
    """.format(control_dict['schema'], table_name)
base.run_db_query(clear_fields_query, control_dict)

print('-------------appending Stats Canada uid data-------------------')
#add the stats can columns/fields to tomtom census table
add_census_fields_query = """ALTER TABLE {0}.{1}
    ADD COLUMN cduid character varying(4),
    ADD COLUMN cdname character varying(40),
    ADD COLUMN csduid character varying(7),
    ADD COLUMN csdname character varying(55),
    ADD COLUMN cmauid character varying(3),
    ADD COLUMN cmaname character varying(100),
    ADD COLUMN ctuid character varying(10),
    ADD COLUMN ctname character varying(10),
    ADD COLUMN pruid character varying(2),
    ADD COLUMN prname character varying(55);
    """.format(control_dict['schema'], table_name)
base.run_db_query(add_census_fields_query, control_dict)

#append the actual data from stats can table to tomtom table
load_census_fields_query = """UPDATE {0}.{1} a
    SET cduid = b.cduid,
        cdname = b.cdname,
        csduid = b.csduid,
        csdname = b.csdname,
        cmauid = b.cmauid,
        cmaname = b.cmaname,
        ctuid = b.ctuid,
        ctname = b.ctname,
        pruid = b.pruid,
        prname = b.prname
    FROM stats_canada{2}.canada_dissem_areas{2} b
    WHERE substring(a.code from length(a.code) -8) = b.dauid;""".format(control_dict['schema'], table_name, census_year)
base.run_db_query(load_census_fields_query, control_dict)

#change the precision of the area, latitude and longetude fiels, add spatial index
change_precision_query = """ALTER TABLE {0}.{1} ALTER COLUMN area TYPE numeric (20,3);
    ALTER TABLE  {0}.{1} ALTER COLUMN longitude TYPE numeric (20,6);
    ALTER TABLE  {0}.{1} ALTER COLUMN latitude TYPE numeric (20,6);
    CREATE INDEX {1}_gix ON {0}.{1} USING gist (geom);
    """.format(control_dict['schema'], table_name)
base.run_db_query(change_precision_query, control_dict)

# run buffer on all the geometry to clean any bad geom
buffer_0_query = """UPDATE {0}.{1} SET geom = st_multi(st_buffer(geom, 0));
    """.format(control_dict['schema'], table_name)
base.run_db_query(buffer_0_query, control_dict)

#QC: test that all features loaded and joined correctly
print('-------------running QC check: feature counts-------------------')
row_count_join_query = """SELECT COUNT(*) FROM {0}.{1} a
    LEFT JOIN stats_canada{2}.canada_dissem_areas{2} b 
    ON a.code = b.dauid 
    WHERE b.dauid IS NOT NULL;""".format(control_dict['schema'], table_name, census_year)
results, notices = base.run_db_query_get_results(row_count_join_query, control_dict)
join_count = results[0][0]
print('Number of features that join to stats can DAs in {}: '.format(table_name) + str(join_count))
shp_feature_count = base.get_shp_feature_count(shp_file)
if join_count != shp_feature_count:
    print('FEATURES COUNTS DO NOT MATCH, NOT ALL DAs LOADED CORRECTLY! FAILING LOAD')
    sys.exit(1)
else:
    print('QC check: feature counts - CHECK PASSED')
    sys.exit(0)