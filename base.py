import set_python_path
import psycopg2, os, csv
from osgeo import ogr, osr

def text_to_list(txt_file):
    #read and text file and return a list
    out_list = []
    with open(txt_file) as file:
        for row in file:
            row2 = row.rstrip()
            out_list.append(row2)
    return out_list

def run_db_query(query, control_dict):
    #executes a sql query on a database with psycopg2
    with psycopg2.connect(dbname=control_dict['database'], port=control_dict['port'], user=control_dict['user'],
                          host=control_dict['host'], password=control_dict['password']) as connection:
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()

def run_db_query_get_results(query, control_dict):
    #executes a sql query on a database with psycopg2
    with psycopg2.connect(dbname=control_dict['database'], port=control_dict['port'], user=control_dict['user'],
                          host=control_dict['host'], password=control_dict['password']) as connection:
        cursor = connection.cursor()
        cursor.execute(query)
        results = cursor.fetchall()
        notices = connection.notices
        return results, notices

def make_list_of_shp(dir):
    #makes a list of all shapefiles in a directory
    shp_list = []
    files = os.listdir(dir)
    for file in files:
        if 'shp' in file:
            filepath = os.path.join(dir, file)
            shp_list.append(filepath)
    return shp_list

def get_shp_field_info(daShapefile):
    #get a list field info for a shapefile
    #retuns list of info for all fields in this format ['name', 'type', width, precision]
    fields = []
    dataSource = ogr.Open(daShapefile)
    daLayer = dataSource.GetLayer(0)
    layerDefinition = daLayer.GetLayerDefn()
    if 'lpc' in daShapefile:
        #for population centers we need a unique id because 'pcuid' is not unique, we create 'mxuid' field
        #'mxuid' is the concatination of 'pruid' + 'pcuid'
        fields.append(['MX_ID', 'String', 6, 0])
    for i in range(layerDefinition.GetFieldCount()):
        field_info = []
        fieldName = layerDefinition.GetFieldDefn(i).GetName()
        field_info.append(fieldName)
        fieldTypeCode = layerDefinition.GetFieldDefn(i).GetType()
        fieldType = layerDefinition.GetFieldDefn(i).GetFieldTypeName(fieldTypeCode)
        field_info.append(fieldType)
        fieldWidth = layerDefinition.GetFieldDefn(i).GetWidth()
        field_info.append(fieldWidth)
        GetPrecision = layerDefinition.GetFieldDefn(i).GetPrecision()
        field_info.append(GetPrecision)
        fields.append(field_info)
    return fields

def make_field_sql_string(fields, table_name):
    #makes a string of all fields and types for use in the make_table_query
    field_sql_string = ""
    #uid_list = ['pruid', 'cduid', 'csuid', 'cmauid']
    for field in fields:
        name_type = field[0:2]  # slice out the field lenth and precision we don't need them
        if name_type[1] == 'String':
            name_type[1] = ' text'  # replace shp "string" type with postgis "text" type
        field_sql_string += name_type[0].lower()
        field_sql_string += name_type[1]
        field_sql_string += ', '
    if table_name == 'canada_dissem_areas': #set disemination areas to NAD83
        field_sql_string += 'geom geometry(MultiPolygon, 4269)'
    elif table_name == 'canada_roads': #set roads to NAD83 and make multiline
        field_sql_string += 'geom geometry(MultiLinestring, 4269)'
    else: #set everything else to wgs84
        field_sql_string += 'geom geometry(MultiPolygon, 4326)'
    return field_sql_string

def get_shp_feature_count(daShapefile):
    #from the ogr cookbook
    #gets a count of the features in a shapefile
    driver = ogr.GetDriverByName('ESRI Shapefile')
    dataSource = driver.Open(daShapefile, 0)  # 0 means read-only. 1 means writeable.
    if dataSource is None:
        print('Could not open %s' % (daShapefile))
    else:
        #print('Opened %s' % (daShapefile))
        layer = dataSource.GetLayer()
        featureCount = layer.GetFeatureCount()
        print("Number of features in %s: %d" % (os.path.basename(daShapefile), featureCount))
        return featureCount

def make_error_pts_shp(shp_path, error_pt_list):
    #from the ogr cookbook
    #makes a point shapefile from the list of bad geometry errors
    new_dir = shp_path + '\\qc_results'
    if not os.path.exists(new_dir):
        os.makedirs(new_dir)
    daShapefile = new_dir + '\\bad_geom_pts.shp'
    driver = ogr.GetDriverByName("ESRI Shapefile")
    if os.path.exists(daShapefile):
        driver.DeleteDataSource(daShapefile)
    data_source = driver.CreateDataSource(daShapefile)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    layer = data_source.CreateLayer(daShapefile, srs, ogr.wkbPoint)
    # Add the fields
    field_name = ogr.FieldDefn("layer", ogr.OFTString)
    field_name.SetWidth(50)
    layer.CreateField(field_name)
    field_region = ogr.FieldDefn("error", ogr.OFTString)
    field_region.SetWidth(25)
    layer.CreateField(field_region)

    for row in error_pt_list:
        # create the feature
        feature = ogr.Feature(layer.GetLayerDefn())
        # set the attrbutes
        feature.SetField("layer", row[0])
        feature.SetField("error", row[1])
        # create the WKT for the feature geometry
        wkt = "POINT(%f %f)" % (float(row[2]), float(row[3]))
        point = ogr.CreateGeometryFromWkt(wkt)
        feature.SetGeometry(point)
        layer.CreateFeature(feature)
        feature.Destroy()
    data_source.Destroy()

def get_shp_info(shp_file):
    #returns a table name and a primary key name for a shapefile from the shp_list
    table_name = None
    pkey_name = None
    if 'lpr' in shp_file:
        table_name = 'canada_prov_terr'
        pkey_name = 'pruid'
    if 'lcd' in shp_file:
        table_name = 'canada_divisions'
        pkey_name = 'cduid'
    if 'lcsd' in shp_file:
        table_name = 'canada_subdivisions'
        pkey_name = 'csduid'
    if 'lcma' in shp_file:
        table_name = 'canada_cma'
        pkey_name = 'cmapuid'
    if 'lda' in shp_file:
        table_name = 'canada_dissem_areas'
        pkey_name = 'dauid'
    if 'lpc' in shp_file:
        table_name = 'canada_pop_centers'
        pkey_name = 'mx_id'
    if 'lrnf' in shp_file:
        table_name = 'canada_roads'
        pkey_name = 'ngduid'
    return table_name, pkey_name

def geometry_qc_test(control_dict, table_name, year, pkey_name):
    bad_geom_query = """SELECT {0} FROM {1}.{2}{3} WHERE ST_IsValid(geom) IS false;
        """.format(pkey_name, control_dict['schema'], table_name, year)
    results = run_db_query_get_results(bad_geom_query, control_dict)
    error_id_list = []
    error_sub_list = []
    for i, ii in zip(results[0], results[1]):
        ii2 = ii.strip('\n')
        if 'point' in ii2:
            error_pt_attr = []
            a = ii2.split(' at or near point ')
            if len(a) != 2:
                pass
            else:
                c = a[1].split(' ')
                lng = c[0]
                lat = c[1]
                e = a[0].split('  ')
                error_type = e[1]
                error_pt_attr.append(table_name)
                error_pt_attr.append(error_type)
                error_pt_attr.append(lng)
                error_pt_attr.append(lat)
                error_id_list.append(i[0])
        error_sub_list.append(error_pt_attr)
        print(ii2)
    print_list = "','".join(error_id_list)
    error_ids_query = pkey_name + " IN ('" + print_list + "')"
    return error_ids_query, error_sub_list, error_id_list
"""
def write_csv(in_list, out_file):
    #writes a list to a csv
    destination = open(out_file, 'w')
    writer = csv.writer(destination, delimiter = ',')
    data = in_list
    for row in data:
        writer.writerow([row])
    destination.close()
"""