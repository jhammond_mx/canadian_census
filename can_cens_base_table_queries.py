"""this script rolls the base canadian census dissemination area table up into the standard census layers for the 
whole of Canada."""

import sys
from base import run_db_query
#from mn_add_census_fields import control_dict
from multiprocessing import Pool

da_table_name = sys.argv[1]
control_dict = {'schema':'mn_canadian_census', 'database':'ace', 'port':'5432', 'user':'ace_procs', 'host':'prod-adb01.maponics.local', 'password':'nosecurity'}

def generate_can_cens_base_tables(control_dict, da_table_name):

    query_list = []  # list to hold the queries that will run to create views
    fields = "geom, cduid, cdname, csduid, csdname, cmauid, cmaname, ctuid, ctname, pruid, prname"

    # create NATION table
    all_can_query = """DROP TABLE IF EXISTS {0}.{1}_cn CASCADE;
        CREATE TABLE {0}.{1}_cn AS
        SELECT '01' as code, 'canada' as areaname, 'cn' as type, st_multi(st_union(geom) as geom
        FROM {0}.{1}
        GROUP BY areaname
        (code character varying(10), areaname character varying(100), type character varying(3) geom geometry(MultiPolygon, 4326))
        ;""".format(control_dict['schema'], da_table_name)
    #query_list.append(all_can_query)

    # create PROVINCE table
    all_pr_query = """DROP TABLE IF EXISTS {0}.{1}_pr CASCADE;
        CREATE TABLE {0}.{1}_pr AS
        SELECT pruid, prname, st_setsrid(st_multi(st_union(geom)), 4326) as geom
        FROM {0}.{1}
        GROUP BY pruid, prname;
        CREATE INDEX {1}_pr_gix ON {0}.{1}_pr USING gist (geom);""".format(control_dict['schema'], da_table_name)
    query_list.append(all_pr_query)

    # create DIVISIONS view
    all_cd_query = """DROP TABLE IF EXISTS {0}.{1}_cd CASCADE;
        CREATE TABLE {0}.{1}_cd AS
        SELECT cduid, cdname, pruid, st_setsrid(st_multi(st_union(geom)), 4326) as geom
        FROM {0}.{1}
        GROUP BY cduid, cdname, pruid
        ;""".format(control_dict['schema'], da_table_name)
    query_list.append(all_cd_query)

    # create SUBDIVISIONS table
    all_cs_query = """DROP TABLE IF EXISTS {0}.{1}_cs CASCADE;
        CREATE TABLE {0}.{1}_cs AS
        SELECT csduid, csdname, pruid, st_setsrid(st_multi(st_union(geom)), 4326) as geom
        FROM {0}.{1}
        GROUP BY csduid, csdname, pruid
        ;""".format(control_dict['schema'], da_table_name)
    query_list.append(all_cs_query)

    # create CMAs table
    regex = r'\\s\\('  # the cmaname field indicates with province part it is in with parenthises, field must be split on ' ('
    #SELECT cmauid as code, (regexp_split_to_array(cmaname, E'{3}'))[1] as areaname, st_setsrid(st_multi(st_union(geom)), 4269) as geom
    all_cm_query = """DROP TABLE IF EXISTS {0}.{1}_cm CASCADE;
        CREATE TABLE {0}.{1}_cm AS
        SELECT cmauid, (regexp_split_to_array(cmaname, E'{2}'))[1] as splitname, st_setsrid(st_multi(st_union(geom)), 4326) as geom
        FROM {0}.{1}
        WHERE cmauid IS NOT NULL
        GROUP BY cmauid, splitname
        ;""".format(control_dict['schema'], da_table_name, regex)
    query_list.append(all_cm_query)

    # create TRACTS table
    all_ct_query = """DROP TABLE IF EXISTS {0}.{1}_ct CASCADE;
        CREATE TABLE {0}.{1}_ct AS
        SELECT ctuid, ctname, pruid, st_setsrid(st_multi(st_union(geom)), 4326) as geom
        FROM {0}.{1}
        WHERE ctuid IS NOT NULL
        GROUP BY ctuid, ctname, pruid
        ;""".format(control_dict['schema'], da_table_name)
    query_list.append(all_ct_query)

    return query_list

def make_base_tables(query):
    """this function wraps base.run_db_query() because pool can only take one arg, control_dict must be passed to run_db_query 
    as a global variable here.
    this must be fixed eventualy"""
    run_db_query(query, control_dict)

def main():
    query_list = generate_can_cens_base_tables(control_dict, da_table_name)
    pool = Pool(4)
    pool.map(make_base_tables, query_list)
    pool.close()
    pool.join()

if __name__ == '__main__':
    main()