"""
 This module writes SQL queries for each view into one list

 Example of what the control_dict  must look like for the generate_can_cens_query_list function:
   control_dict = {'schema':'mn_canadian_census', 'database':'ace', 'port':'5432', 'user':'ace_procs', 'host':'prod-adb01.maponics.local', 'password':'nosecurity'}   
"""
import re

pr_code_list = ['48', '59', '47', '46', '35', '24', '13', '12', '11', '10', '60', '61', '62']  # pruids
pr_abbr_list = ['ab', 'bc', 'sk', 'mb', 'on', 'qc', 'nb', 'ns', 'pe', 'nl', 'yt', 'nt', 'nu']  # pr abreviations for file naming
pr_code_list2 = ['48', '59', '47', '46', '35', '24', '13', '12', '10']  # pruids, no territories or PEI !!!
pr_abbr_list2 = ['ab', 'bc', 'sk', 'mb', 'on', 'qc', 'nb', 'ns', 'nl']  # pr abreviations no territories or PEI
pr_code_list3 = ['48', '59', '47', '46', '35', '24', '13', '12', '11', '10']  # pruids, no territories !!!
pr_abbr_list3 = ['ab', 'bc', 'sk', 'mb', 'on', 'qc', 'nb', 'ns', 'pe', 'nl']  # pr abreviations no territories
cm_code_list = ['462', '535', '933']  # cmuids
cm_abbr_list = ['mont', 'tor', 'van']  # cma abreviations for file naming

def generate_can_cens_query_list(do_list, control_dict, version, da_table_name, dump_dir, year):

    tab_list_file = dump_dir + r'\tab_list.txt' #file thats lists all tab files created in teh run, to be used by MB script
    year2 = year[-2:]
    query_list = []  # list to hold the queries that will run to create views

    # ------------------layers by nation-------------------------------------------------------
    # create NATION view
    all_can_query = """DROP MATERIALIZED VIEW IF EXISTS {0}.cn{1}cn{3};
        CREATE MATERIALIZED VIEW {0}.cn{1}cn{3} AS
        SELECT '01' as code, 'Canada' as areaname, 'can' as type, st_setsrid(st_multi(st_union(geom)), 4269) as geom
        FROM {0}.{2}_pr
        GROUP BY areaname;""".format(control_dict['schema'], year2, da_table_name, version)
    if 'cn_cn' in do_list:
        query_list.append(all_can_query)

    # create PROVINCE view
    all_pr_query = """CREATE OR REPLACE VIEW {0}.cn{1}pr{3} AS
        SELECT pruid as code, prname as areaname, 'pr' as type, st_setsrid(st_multi(geom), 4269) as geom
        FROM {0}.{2}_pr
        ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, version)
    if 'cn_pr' in do_list:
        query_list.append(all_pr_query)

    # create DIVISIONS view
    all_cd_query = """CREATE OR REPLACE VIEW {0}.cn{1}cd{3} AS
        SELECT cduid as code, cdname as areaname, 'cd' as type, st_setsrid(st_multi(geom), 4269) as geom
        FROM {0}.{2}_cd
        ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, version)
    if 'cn_cd' in do_list:
        query_list.append(all_cd_query)

    # create SUBDIVISIONS view
    all_sd_query = """CREATE OR REPLACE VIEW {0}.cn{1}cs{3} AS
        SELECT csduid as code, csdname as areaname, 'csd' as type, st_setsrid(st_multi(geom), 4269) as geom
        FROM {0}.{2}_cs
        ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, version)
    if 'cn_cs' in do_list:
        query_list.append(all_sd_query)

    # create the DISSEMINATION AREAS view from TOM TOM DATA
    da_can_query = """CREATE OR REPLACE VIEW {0}.cn{1}da{3} AS
        SELECT  code, code as areaname, 'da' as type, area, latitude, longitude, st_setsrid(st_multi(geom), 4269) as geom
        FROM {0}.{2}
        ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, version)
    if 'cn_da' in do_list:
        query_list.append(da_can_query)

    # create CMAs view
    #regex = r'\\s\\('  # the cmaname field indicates with province part it is in with parenthises, field must be split on ' ('
    all_cm_query = """CREATE OR REPLACE VIEW {0}.cn{1}cm{3} AS
        SELECT cmauid as code, splitname as areaname, 'cma' as type, st_setsrid(st_multi(geom), 4269) as geom
        FROM {0}.{2}_cm
        ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, version)
    if 'cn_cm' in do_list:
        query_list.append(all_cm_query)

    # create TRACTS view
    all_ct_query = """CREATE OR REPLACE VIEW {0}.cn{1}ct{3} AS
        SELECT ctuid as code, ctuid as areaname, 'ct' as type, st_setsrid(st_multi(geom), 4269) as geom
        FROM {0}.{2}_ct
        ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, version)
    if 'cn_ct' in do_list:
        query_list.append(all_ct_query)

    # create the DISSEMINATION AREAS table from STATS CANADA DATA  -------- this is for straight stats can ONLY
    da_can_query = """CREATE OR REPLACE VIEW {0}.cn{1}da AS 
        SELECT  dauid as code, dauid as areaname,
            (st_area(geom :: geography)/1000) as area,
            st_y(st_centroid(geom)) as latitude,
            st_x(st_centroid(geom)) as longitude,
            geom
        FROM {0}.canada_dissem_areas{2};""".format(control_dict['schema'], year2, year)
    # query_list.append(da_can_query) -------------------------------------- this is for straight stats can ONLY

    # create AGLOMERATED DISSEM AREAS table
    all_ada_query = """DROP MATERIALIZED VIEW IF EXISTS {0}.cn{1}ada;
        CREATE MATERIALIZED VIEW {0}.cn{1}ada AS
        SELECT adauid as code, adauid as areaname, st_setsrid(st_multi(st_union(geom)), 4269) as geom
        FROM {0}.canada_dissem_areas{2}
        WHERE adauid IS NOT NULL
        GROUP BY code, areaname;""".format(control_dict['schema'], year2, year)
    # query_list.append(all_ada_query)

    # create the ROADS table
    rnf_can_query = """CREATE OR REPLACE VIEW {0}.cn{1}rnf AS
        SELECT*
        FROM {0}.canada_roads{2};""".format(control_dict['schema'], year2, year)
    # query_list.append(rnf_can_query)

    # ---------------------------layers by province---------------------------------------------
    # create the PROVINCE view by province
    for pid, pabr in zip(pr_code_list, pr_abbr_list):
        pr_pr_query = """CREATE OR REPLACE VIEW {0}.{3}{1}pr{5} AS
            SELECT pruid as code, prname as areaname, 'pr' as type, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{2}_pr
            WHERE pruid = '{4}';""".format(control_dict['schema'], year2, da_table_name, pabr, pid, version)
        if 'pr_pr' in do_list:
            query_list.append(pr_pr_query)

    # create the DIVISION views by province
    for pid, pabr in zip(pr_code_list, pr_abbr_list):
        cd_pr_query = """CREATE OR REPLACE VIEW {0}.{3}{1}cd{5} AS
            SELECT cduid as code, cdname as areaname, 'cd' as type, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{2}_cd
            WHERE pruid = '{4}'
            ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, pabr, pid, version)
        if 'pr_cd' in do_list:
            query_list.append(cd_pr_query)

    # create the SUBDIVISION views by province
    for pid, pabr in zip(pr_code_list, pr_abbr_list):
        csd_pr_query = """CREATE OR REPLACE VIEW {0}.{3}{1}cs{5} AS
            SELECT csduid as code, csdname as areaname, 'csd' as type, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{2}_cs
            WHERE pruid = '{4}'
            ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, pabr, pid, version)
        if 'pr_cs' in do_list:
            query_list.append(csd_pr_query)

    # create the DISSEMINATION AREAS views by province for TOM TOM DATA
    for pid, pabr in zip(pr_code_list, pr_abbr_list):
        da_pr_query = """CREATE OR REPLACE VIEW {0}.{3}{1}da{5} AS
            SELECT  code, code as areaname, 'da' as type,
                area, latitude, longitude, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{2}
            WHERE pruid = '{4}'
            ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, pabr, pid, version)
        if 'pr_da' in do_list:
            query_list.append(da_pr_query)

    # create the TRACTS views by province
    for pid, pabr in zip(pr_code_list2, pr_abbr_list2):
        ct_pr_query = """CREATE OR REPLACE VIEW {0}.{3}{1}ct{5} AS
            SELECT ctuid as code, ctuid as areaname, 'ct' as type, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{2}_ct
            WHERE pruid = '{4}'
            ORDER BY code;""".format(control_dict['schema'], year2, da_table_name, pabr, pid, version)
        if 'pr_ct' in do_list:
            query_list.append(ct_pr_query)

    #create the CMA views by province
    for pid, pabr in zip(pr_code_list3, pr_abbr_list3):
        cm_pr_query = """CREATE OR REPLACE VIEW {0}.{3}{1}cm{5} AS
            SELECT cmauid as code, splitname as areaname, 'cma' as type, st_setsrid(st_multi(a.geom), 4269) as geom
            FROM {0}.{2}_cm a
            JOIN (select geom from stats_canada2016.canada_prov_terr2016 where pruid = '{4}') prov
            ON prov.geom && a.geom AND ST_Relate(prov.geom, a.geom, '2********');
            """.format(control_dict['schema'], year2, da_table_name, pabr, pid, version)
        if 'pr_cm' in do_list:
            query_list.append(cm_pr_query)

    # create the DISSEMINATION AREAS views by province for STATS CANDADA DATA  -----this is for straight stats can ONLY
    for pid, pabr in zip(pr_code_list, pr_abbr_list):
        da_pr_query = """CREATE OR REPLACE VIEW {0}.{3}{1}da AS
            SELECT  dauid as code, dauid as areaname,
                (st_area(geom :: geography)/1000) as area,
                st_y(st_centroid(geom)) as latitude,
                st_x(st_centroid(geom)) as longitude,
                geom
            FROM {0}.{2}
            WHERE pruid = '{4}';""".format(control_dict['schema'], year2, da_table_name, pabr, pid)
        #query_list.append(da_pr_query)

    # create the AGLOMERATED DISSEM AREAS  views by province
    for pid, pabr in zip(pr_code_list, pr_abbr_list):
        ada_pr_query = """DROP MATERIALIZED VIEW IF EXISTS {0}.{3}{1}ada;
            CREATE MATERIALIZED VIEW {0}.{3}{1}ada AS
            SELECT adauid as code, adauid as areaname, st_setsrid(st_multi(st_union(geom)), 4269) as geom
            FROM {0}.canada_dissem_areas{2}
            WHERE pruid = '{4}' AND adauid IS NOT NULL
            GROUP BY code, areaname;""".format(control_dict['schema'], year2, year, pabr, pid)
        #query_list.append(ada_pr_query)

    # create the ROADS views by province
    for pid, pabr in zip(pr_code_list, pr_abbr_list):
        rnf_pr_query = """CREATE OR REPLACE VIEW {0}.{3}{1}rnf AS
                SELECT*
                FROM {0}.canada_roads{2}
                WHERE pruid_l = '{4}' OR pruid_r = '{4}';""".format(control_dict['schema'], year2, year, pabr, pid)
        #query_list.append(rnf_pr_query)


    # --------------------------layers by 3 CMAs -------------------------------------------------
    # create the CMA view by CMA
    for mid, pabr in zip(cm_code_list, cm_abbr_list):
        cma_cma_query = """CREATE OR REPLACE VIEW {0}.{3}{1}cm{2} AS
            SELECT cmauid as code, cmaname as areaname, 'cma' as type, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{5}_cm
            WHERE cmauid = '{4}';""".format(control_dict['schema'], year2, version, pabr, mid, da_table_name)
        if 'cm_cm' in do_list:
            query_list.append(cma_cma_query)

    # create the DIVISION views by CMA
    for mid, pabr in zip(cm_code_list, cm_abbr_list):
        cd_cma_query = """CREATE OR REPLACE VIEW {0}.{2}{5}cd{4} AS
            SELECT cduid as code, cdname as areaname, 'cd' as type, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{1}_cd
            WHERE cmauid = '{3}'
            ORDER BY code;""".format(control_dict['schema'], da_table_name, pabr, mid, version, year2)
        if 'cm_cd' in do_list:
            query_list.append(cd_cma_query)

    # create the SUBDIVISION views by CMA
    for mid, pabr in zip(cm_code_list, cm_abbr_list):
        cs_cma_query = """CREATE OR REPLACE VIEW {0}.{2}{5}cs{4} AS
            SELECT csduid as code, csdname as areaname, 'csd' as type, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{1}_cs
            WHERE cmauid = '{3}'
            ORDER BY code;""".format(control_dict['schema'], da_table_name, pabr, mid, version, year2)
        if 'cm_cs' in do_list:
            query_list.append(cs_cma_query)

    # create the DISSEMINATION AREAS views by CMA for TOM TOM DATA
    for mid, pabr in zip(cm_code_list, cm_abbr_list):
        da_cma_query = """CREATE OR REPLACE VIEW {0}.{2}{5}da{4} AS
            SELECT  code, code as areaname, 'da' as type,
                area, latitude, longitude, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{1}
            WHERE cmauid = '{3}'
            ORDER BY code;""".format(control_dict['schema'], da_table_name, pabr, mid, version, year2)
        if 'cm_da' in do_list:
            query_list.append(da_cma_query)
        # base.run_db_query(da_cma_query, control_dict)

    # create the TRACTS views by CMA
    for mid, pabr in zip(cm_code_list, cm_abbr_list):
        ct_cma_query = """CREATE OR REPLACE VIEW {0}.{2}{5}ct{4} AS
            SELECT ctuid as code, ctuid as areaname, 'ct' as type, st_setsrid(st_multi(geom), 4269) as geom
            FROM {0}.{1}_ct
            WHERE cmauid = '{3}'
            ORDER BY code;""".format(control_dict['schema'], da_table_name, pabr, mid, version, year2)
        if 'cm_ct' in do_list:
            query_list.append(ct_cma_query)

    # create the DISSEMINATION AREAS views by CMA for STATS CANADA DATA  ----------this is for straight stats can ONLY
    for mid, pabr in zip(cm_code_list, cm_abbr_list):
        da_cma_query = """CREATE OR REPLACE VIEW {0}.{2}_da AS
            SELECT  dauid as code, dauid as areaname,
                (st_area(geom :: geography)/1000) as area,
                st_y(st_centroid(geom)) as latitude,
                st_x(st_centroid(geom)) as longitude,
                geom
            FROM {0}.canada_dissem_areas{1}
            WHERE cmauid = '{3}';""".format(control_dict['schema'], year, pabr, mid)
        # query_list.append(da_cma_query)
        # base.run_db_query(da_cma_query, control_dict)

    # create the AGLOMERATED DISSEM AREAS views by CMA
    for mid, pabr in zip(cm_code_list, cm_abbr_list):
        ada_cma_query = """DROP MATERIALIZED VIEW IF EXISTS {0}.{2}_ada;
            CREATE MATERIALIZED VIEW {0}.{2}_ada AS
            SELECT adauid as code, adauid as areaname, st_setsrid(st_multi(st_union(geom)), 4269) as geom
            FROM {0}.{1}
            WHERE cmauid = '{3}'
            GROUP BY code, areaname;""".format(control_dict['schema'], da_table_name, pabr, mid)
        # query_list.append(ada_cma_query)
        # base.run_db_query(ada_cma_query, control_dict)

    # create the ROADS views by CMA
    for mid, pabr in zip(cm_code_list, cm_abbr_list):
        rnf_cma_query = """CREATE OR REPLACE VIEW {0}.{2}_rnf AS
            SELECT*
            FROM {0}.canada_roads{1}
            WHERE cmauid_l = '{3}' OR cmauid_r = '{3}';""".format(control_dict['schema'], year, pabr, mid)
        # query_list.append(rnf_cma_query)

    #create txt file thats lists all the eventual tab file names -------------------------------------------
    #this will be used by the MB script for "cleaning" of the tab files
    f = open(tab_list_file, 'w')
    for x in query_list:
        name = re.search('\.(.*?) AS', x).group(1)
        full_path = dump_dir + '\\' + name + '.TAB'
        f.write("{}\n".format(full_path))
    f.close()

    return query_list