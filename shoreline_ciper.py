import psycopg2
import base

control_dict = {'host':'prod-adb01.maponics.local', 'database':'ace', 'user':'ace_procs', 'port':'5432', 'password':'nosecurity', 'schema':'jhammond'}
year = 2016
table = 'tester'

#get list of all da polys to loop though
coastal_da_list = []
coastal_da_list_query  = """SELECT ogc_fid
    FROM {0}.{1};""".format(control_dict['schema'], table)
results, notices = base.run_db_query_get_results(coastal_da_list_query, control_dict)
for i in results:
    coastal_da_list.append(i[0])

#calculate area of polys
calc_area_query = """ALTER TABLE {0}.{1} ADD COLUMN area float;
    UPDATE {0}.{1} SET area = ST_Area(wkb_geometry::geography);""".format(control_dict['schema'], table)
#base.run_db_query(calc_area_query, control_dict)

#select all slivers
for da in coastal_da_list[:100]:
    code_change_list = [] #sublist is as follows (code of the 'mother' poly, fid of the sliver poly, code of the sliver poly)
    code_to_change_query = """WITH p AS (SELECT ogc_fid, code, wkb_geometry AS geom FROM {0}.{1} WHERE ogc_fid = {2}),
        a AS (SELECT code, ogc_fid, wkb_geometry AS geom FROM {0}.{1} WHERE area < 15000)
        SELECT p.code, a.ogc_fid, a.code
        FROM a, p
        WHERE st_intersects(p.geom, a.geom) AND p.ogc_fid != a.ogc_fid;""".format(control_dict['schema'], table, da)
    results2, notices = base.run_db_query_get_results(code_to_change_query, control_dict)
    print('-------------------------------------------------------------------------------------------------------------')
    print(da)
    print(results2)
    if len(results2)<1:
        print('no slivers')
    else:
        where_string = """WHERE ogc_fid IN ("""
        for fid in results2:
            where_string += str(fid[1])
            where_string += ', '
        where_string = where_string[:-2]
        where_string += ')'
        print(where_string)
        new_code = results2[0][0]
        update_code_query = """UPDATE {0}.{1}
            SET code = {2};""".format(control_dict['schema'], table, new_code)
        #base.run_db_query(update_code_query, control_dict)
