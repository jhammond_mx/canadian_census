import base, sys
from util import control

control_file = sys.argv[1]
control_dict = control.get_control_dict(control_file)
year = sys.argv[2]
raw_data_dir = sys.argv[3]
tiger_prime_control_file = sys.argv[4]
tiger_prime_dict = control.get_control_dict(tiger_prime_control_file)

#load the canadian subdivisions layer in ace into prime add some new MX style fields
ace2prime_query = """DROP TABLE IF EXISTS {6}.can_census_subdivision_deagg_{1};

    CREATE TABLE {6}.can_census_subdivision_deagg_{1} AS
    SELECT ace.csduid, ace.csdname, ace.csdtype, ace.pruid, ace.prname, ace.cduid, ace.cdname, ace.cdtype,
         ace.cmauid, ace.cmaname, ace.sactype, ace.geom, ace.prabbr, ace.cntrysub_e, ace.cntrysub_n,
         ace.cntrysub_r, ace.country_e, ace.country_n, ace.country_r
    FROM dblink('dbname={2} user={3} host={4} password={5}',
        $$SELECT csduid, csdname, csdtype, pruid, prname, cduid, cdname, cdtype, cmauid, cmaname, sactype, geom,
        CASE
            WHEN pruid = '59' THEN 'BC'
            WHEN pruid = '48' THEN 'AB'
            WHEN pruid = '47' THEN 'SK'
            WHEN pruid = '46' THEN 'MB'
            WHEN pruid = '35' THEN 'ON'
            WHEN pruid = '24' THEN 'QC'
            WHEN pruid = '13' THEN 'NB'
            WHEN pruid = '12' THEN 'NS'
            WHEN pruid = '11' THEN 'PE'
            WHEN pruid = '10' THEN 'NL'
            WHEN pruid = '60' THEN 'YT'
            WHEN pruid = '61' THEN 'NT'
            WHEN pruid = '62' THEN 'NU'
        END AS prabbr,
        split_part(prname, ' / ', 1) AS cntrysub_e,
        CASE
            WHEN pruid = '24' THEN split_part(prname, ' / ', 2)
            ELSE split_part(prname, ' / ', 1)
        END AS cntrysub_n,
        NULL AS cntrysub_r,
        'Canada' AS country_e,
        'Canada' AS country_n,
        NULL AS country_r
        FROM {0}.canada_subdivisions{1}$$) AS ace
    (csduid character varying(7),
     csdname character varying(100),
     csdtype character varying(3),
     pruid character varying(2),
     prname character varying(100),
     cduid character varying(4),
     cdname character varying(100),
     cdtype character varying(3),
     cmauid character varying(3),
     cmaname character varying(100),
     sactype character varying(1),
     geom geometry(multipolygon, 4326),
     prabbr character varying(2),
     cntrysub_e character varying(100),
     cntrysub_n character varying(100),
     cntrysub_r character varying(100),
     country_e character varying(100),
     country_n character varying(100),
     country_r character varying(100));

     CREATE INDEX can_census_subdivision_deagg_{1}_geom_idx
     ON {6}.can_census_subdivision_deagg_{1} USING gist (geom);
     """.format(control_dict['schema'], year, control_dict['database'], control_dict['user'],
                control_dict['host'], control_dict['password'], tiger_prime_dict['schema'])
base.run_db_query(ace2prime_query, tiger_prime_dict)

pkey_query = """ALTER TABLE {1}.can_census_subdivision_deagg_{0}
     ADD CONSTRAINT can_census_subdivision_deagg_{0}_pkey PRIMARY KEY (csduid);
     """.format(year, tiger_prime_dict['schema'])
base.run_db_query(pkey_query, tiger_prime_dict)

#do some testing to make sure all the subdivision records loaded to prime from ace
ace_row_count_query = """SELECT COUNT(*) FROM {0}.canada_subdivisions{1};
            """.format(control_dict['schema'], year)
ace_row_count_raw = base.run_db_query_get_results(ace_row_count_query, control_dict)
ace_row_count = ace_row_count_raw[0][0][0]
ace_row_count_msg = 'Number of rows in table ' + control_dict['schema'] + '.canada_subdivisions' + year + ': '
print(ace_row_count_msg, ace_row_count)

prime_row_count_query = """SELECT COUNT(*) FROM {1}.can_census_subdivision_deagg_{0};
            """.format(year, tiger_prime_dict['schema'])
prime_row_count_raw = base.run_db_query_get_results(prime_row_count_query, tiger_prime_dict)
prime_row_count = prime_row_count_raw[0][0][0]
prime_row_count_msg = 'Number of rows in table ' + tiger_prime_dict['schema'] + '.can_census_subdivision_deagg_' + year + ': '
print(prime_row_count_msg, prime_row_count)

if ace_row_count != prime_row_count:
    print(tiger_prime_dict['schema'], '.can_census_subdivision_deagg_', year, ' MISSING FEATURES!!!')
    sys.exit(1)
else:
    print('Canadian subdivisions layer ace to prime load complete!!!')
    sys.exit(0)

