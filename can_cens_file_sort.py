"""This script sorts all the files in the dump directory into a standard set up subfolders"""

import os, sys, shutil

dump_dir = sys.argv[1]
print(dump_dir)

def files(path):
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            yield file

def mkdir(new_dir):
    if not os.path.exists(new_dir):
        os.makedirs(new_dir, exist_ok=True)

def move(source_path, dest_dir, sub_dir, file_name):
    dest_path = dest_dir + '\\' + sub_dir + '\\' + file_name
    shutil.move(source_path, dest_path)

def main(dump_dir):

    # make all the subdirectories

    Canada_dir = dump_dir + '\\' + 'Canada'
    mkdir(Canada_dir)
    print(Canada_dir)

    Prov_dir = dump_dir + '\\' + 'Census11_Bndys_by_province'
    pr_layer_list = ['pr', 'cd', 'cs', 'da', 'ct', 'cm']
    mkdir(Prov_dir)
    for i in pr_layer_list:
        new_dir = Prov_dir + '\\' + i
        mkdir(new_dir)

    """
    CMA_dir = dump_dir + '\\' + 'Census11_Bndys_by_CMA'
    cma_layer_list = ['Montreal_CMA', 'Toronto_CMA', 'Vancouver_CMA']
    mkdir(CMA_dir)
    for ii in cma_layer_list:
        new_dir2 = CMA_dir + '\\' + ii
        mkdir(new_dir2)
    """

    # move the files
    for f in files(dump_dir):
        spath = dump_dir + '\\' + f

        # move the nationwide files
        if 'cn11' in f:
            dpath = Canada_dir + '\\' + f
            shutil.move(spath, dpath)
            continue
        """
        # move the cma wide files
        if 'mont11' in f:
            move(spath, CMA_dir, 'Montreal_CMA', f)
            continue
        if 'tor11' in f:
            move(spath, CMA_dir, 'Toronto_CMA', f)
            continue
        if 'van11' in f:
            move(spath, CMA_dir, 'Vancouver_CMA', f)
            continue
        """
        # move the province wide files
        if '11pr' in f:
            move(spath, Prov_dir, 'pr', f)
        if '11cd' in f:
            move(spath, Prov_dir, 'cd', f)
        if '11cs' in f:
            move(spath, Prov_dir, 'cs', f)
        if '11ct' in f:
            move(spath, Prov_dir, 'ct', f)
        if '11cm' in f:
            move(spath, Prov_dir, 'cm', f)
        if '11da' in f:
            move(spath, Prov_dir, 'da', f)

if __name__ == '__main__':
    main(dump_dir)
    sys.exit(0)