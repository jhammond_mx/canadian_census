"""This script creates views of the following canadian census layers for different levels of geography

at a nationwide geography:
nation, province, dissemination areas, aglomerated dissemination areas, subdivisions, divisions, CMAs, tracts, roads

at the province geography:
dissemination areas, aglomerated dissemination areas, subdivisions, divisions, tracts, roads

at the CMA geography for montreal, toronto and vancouver:
dissemination areas, aglomerated dissemination areas, subdivisions, divisions, tracts, roads

The code then dumps a tab file for each view created"""

import psycopg2, re, subprocess, sys
import base, can_cens_queries
from multiprocessing import Pool

#control_dict = control.get_control_dict(control_file)
control_dict = {'schema':'mn_canadian_census', 'database':'ace', 'port':'5432', 'user':'ace_procs', 'host':'prod-adb01.maponics.local', 'password':'nosecurity'}
version = sys.argv[1] #'2017.03'
da_table_name = sys.argv[2] #'mn_unshorlined_2201612'
dump_dir = sys.argv[3] #r'\\tro-share01\datawarehouse\warehouse\PB-InhouseDemographicData\Canada\Geography\C11_TOMTOM_BOUNDARIES\test_dump'
layer_control_file = sys.argv[4] #r'\\prodfs01.maponics.local\Resource-Library\Processing\can_cens_unshor_layer_control.txt'
census_year = '2011'

#from the layer control files make list of the views and tab files that will need to be created


#execute the querys and write each resulting view to a TAB file using ogr2ogr
def run_query(query):
    with psycopg2.connect(dbname=control_dict['database'], port='5432', user='ace_procs', host=control_dict['host'], password='nosecurity') as connection:
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
    name = re.search('\.(.*?) AS', query).group(1)
    print('View ' + name + ': Created')

    #NOTE! the PG argument of the ogr2ogr control string needs '' for use in linux example: dbname='ace'
    pg_string = """PG:"dbname=ace host={0} port=5432 user=ace_procs password=nosecurity """.format(control_dict['host'])
    pg_string += 'schemas={0}"'.format(control_dict['schema'])
    cmd_list = ['set', 'PGCLIENTENCODING=LATIN1&ogr2ogr', '-f', '"MapInfo File"', dump_dir, pg_string, name] #remove 'set' when runing on linux

    cmd = " ".join(cmd_list)
    print(cmd)
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    p.wait()
    rc = p.returncode

    if rc != 0:
        print(name + ' !!!!!!failed to convert!!!!!!!')
    else:
        print(name + '.TAB Created')

#run teh main function in a pool
def main():
    do_list = base.text_to_list(layer_control_file)
    l = can_cens_queries.generate_can_cens_query_list(do_list, control_dict, version, da_table_name, dump_dir, census_year)
    pool = Pool(4)
    pool.map(run_query, l)
    pool.close()
    pool.join()

if __name__ == '__main__':
    main()