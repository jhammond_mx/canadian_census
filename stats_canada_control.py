import set_python_path
from util import control
import sys
import base
import subprocess

control_file = sys.argv[1]
control_dict = control.get_control_dict(control_file)
year = sys.argv[2]
raw_data_dir = sys.argv[3]

if __name__ == "__main__":
    make_schema_query = """CREATE SCHEMA IF NOT EXISTS {0};
        GRANT USAGE ON SCHEMA {0} TO so_user;
        ALTER DEFAULT PRIVILEGES IN SCHEMA {0}
        GRANT SELECT ON TABLES TO so_user;""".format(control_dict['schema'])
    base.run_db_query(make_schema_query, control_dict)

    shp_list = base.make_list_of_shp(raw_data_dir)
    for i in shp_list:
        #loop through the shapefiles
        #create the postgis tables then load in the shapefiles with ogr2ogr
        table_name, pkey_name = base.get_shp_info(i)
        fields = base.get_shp_field_info(i)
        field_sql_string = base.make_field_sql_string(fields, table_name)
        if table_name == None:
            # skip any shapefiles that we didn't load
            continue

        make_table_query = """DROP TABLE IF EXISTS {0}.{1}{2};
            CREATE TABLE {0}.{1}{2}
            ({3});""".format(control_dict['schema'], table_name, year, field_sql_string)
        base.run_db_query(make_table_query, control_dict)

        grant_access_query = """GRANT SELECT ON TABLE {0}.{1}{2} TO so_user;
            GRANT ALL ON TABLE {0}.{1}{2} TO ace_procs;
            """.format(control_dict['schema'], table_name, year)
        base.run_db_query(grant_access_query, control_dict)

        #build a command string to run ogr2ogr on the shapefiles
        db_conection_raw = '"PG:host=%s user=%s password=%s dbname=%s"'
        db_connection = db_conection_raw % (control_dict['host'], control_dict['user'], control_dict['password'], control_dict['database'])
        full_table_name = control_dict['schema'] + '.' + table_name + year
        ogr_command_string = 'ogr2ogr -append -a_srs EPSG:42304 -t_srs EPSG:4326 -nlt PROMOTE_TO_MULTI '
        ogr_command_string += '-lco "SCHEMA=stats_canada2016" -lco ENCODING=UTF-8 -f PostgreSQL '
        ogr_command_string += db_connection
        ogr_command_string +=  ' -nln '
        ogr_command_string += full_table_name
        ogr_command_string += ' '
        ogr_command_string += i

        #run the ogr2ogr command
        try:
            p = subprocess.Popen(ogr_command_string, stdout=subprocess.DEVNULL)
            p.wait()
            retval = p.returncode
            if retval != 0:
                error = "ERROR: Failure to append %s to table %s!\n" % (i, full_table_name)
                error += "Command: %s \n" % ogr_command_string
                print(error)
                # errors.append(error)
            else:
                msg = '-------------APPENDED: ' + table_name + ' SUCCESSFULLY------------'
                print(msg)
        except Exception as e:
            error = "ERROR IN LAYER : %s\n" % i
            error += str(e)
            print(error)
            #errors.append(error)

        #make the primary keys for the tables
        #we do this now because for population centers we need to populate the pkey field ('mx_id') after the ogr -append
        if 'lpc' in i:
            #update the pop centers table to set 'mx_id'
            mx_id_query = """UPDATE {0}.{1}{2} SET mx_id = pruid || pcuid;
            """.format(control_dict['schema'], table_name, year)
            base.run_db_query(mx_id_query, control_dict)
        pkey_query = """ALTER TABLE {0}.{1}{2}
            ADD CONSTRAINT {1}{2}_pkey PRIMARY KEY ({3});
            """.format(control_dict['schema'], table_name, year, pkey_name)
        base.run_db_query(pkey_query, control_dict)

    sys.exit(0)